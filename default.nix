with (import <nixpkgs> {});
let
  name = "zr-hardware-sensors";
  gems = bundlerEnv {
    name = "${name}-env";
    inherit ruby;
    gemdir = ./.;
  };
in stdenv.mkDerivation {
  name = name;
  buildInputs = [gems ruby xorg.xrandr xorg.xinput];
  source = ./.;
}
