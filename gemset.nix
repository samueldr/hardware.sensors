{
  rake = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0jcabbgnjc788chx31sihc5pgbqnlc1c75wakmqlbjdm8jns2m9b";
      type = "gem";
    };
    version = "10.5.0";
  };
  ruby-dbus = {
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "010cxvndpgggakjnfdiawjldq656601gd9nc95cvrzsbba12a58x";
      type = "gem";
    };
    version = "0.14.0";
  };
  #zr-hardware-sensors = {
  #  dependencies = ["ruby-dbus"];
  #};
}
