# FIXME : split input thing in a separate library.
require "shellwords"

module Zr
	module Hardware
	end
end

module Zr::Hardware::Inputs
	@pointers = {}
	@touchscreens = {}

	# ewwww, don't tell anyone that this is the magic sauce.
	def self.xinput(*params)
		params = ["xinput"].concat(params)
		`#{Shellwords.join(params)}`
	end

	def self.init()
		out = `xinput`

		# Passes through xinput error.
		# Mostly relevant.
		unless $?.exitstatus == 0 then
			$stderr.print out
			exit 1
		end

		refresh
	end

	def self.refresh()
		@pointers = pointer_ids.map do |id|
			data = xinput("list", id).split("\n")
			[id, Zr::Hardware::Pointer.new(data)]
		end
			.to_h

		@touchscreens = @pointers.select do |id, pointer|
			pointer.touch
		end
			.to_h
	end

	def self.pointer_ids()
		# ewww
		ids = `xinput list`
			.split("\n").map { |line| line.split("\t") }
			.select { |data| data[2].match(/slave\s+pointer/) }
			.map { |data| data[1].split("=").last }
	end

	def self.touchscreens
		@touchscreens
	end
end

class Zr::Hardware::Pointer
	attr_accessor :touch
	attr_accessor :id
	def initialize(data)
		first = data.shift
		@name = first.split("\tid=").first.strip
		@id = first.split("\tid=").last.split(/\s/).first.strip
		@data = data
		@touch = data.select { |line| line.match(/XITouchClass/) }.length > 0
	end

	def map_to_output(output)
		Zr::Hardware::Inputs.xinput("--map-to-output", @id, output)
	end
end

# Initial "init", do not rely on its information.
Zr::Hardware::Inputs.init
