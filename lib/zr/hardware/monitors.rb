# FIXME : split monitor thing in a separate library.
require "shellwords"

module Zr
	module Hardware
	end
end

module Zr::Hardware::Monitors
	INTERNAL_TYPES = ["eDP", "DSI"]

	# All displays
	@displays = {}

	# Copy of "internal" displays. (Laptop/tablet screens)
	@internal_displays = {}

	# ewwww, don't tell anyone that this is the magic sauce.
	def self.xrandr(*params)
		params = ["xrandr"].concat(params)
		`#{Shellwords.join(params)}`
	end

	def self.init()
		randr = `xrandr`

		# Passes through xrandr error.
		# Mostly relevant.
		unless $?.exitstatus == 0 then
			$stderr.print randr
			exit 1
		end

		refresh
	end

	def self.refresh()
		randr = `xrandr`
		displays = randr.split("\n").grep(/^[^\s]/)
		screen = displays.shift

		@displays = displays
			.select { |line| !line.match(/^VIRTUAL/) } # Discards virtual displays
			.map do |line|
				name = line.split(/\s+/)[0]
				[name, Monitor.new(name, line)]
			end.to_h
			.group_by { |name, monitor| monitor.status }
			.map { |k, o| [k, o.to_h] }.to_h # Re-hydrates to hashes.

		@displays.each do |type, group|
			group.each do |id, value|
				name = id.gsub(/[^a-zA-Z]/, "")
				if INTERNAL_TYPES.include?(name)
					@internal_displays[id] = value
				end
			end
			@internal_displays 
		end
	end

	def self.internal_displays()
		@internal_displays
	end
end

class Zr::Hardware::Monitors::Monitor
	attr_accessor :status
	def initialize(name, line)
		@name = name
		@line = line
		@status = line.split(/\s+/)[1]
	end

	def rotate(orientation)
		Zr::Hardware::Monitors.xrandr("--output", @name, "--rotate", orientation)
	end
end

# Initial "init", do not rely on its information.
Zr::Hardware::Monitors.init
