require "zr/hardware/monitors"
require "zr/hardware/inputs"
require "dbus"

module Zr
	module Hardware
	end
end

# Handles the funny business of listening to sensors.
class Zr::Hardware::Sensors
	RANDR = {
		"normal" => "normal",
		"right-up" => "right",
		"left-up" => "left",
		"bottom-up" => "inverted",
	}
	# FIXME : detect presence of stuff on bus. Exit cleanly when not available.
	def initialize()
		@bus = DBus::SystemBus.instance
		@service = @bus.service("net.hadess.SensorProxy")
		object = @service.object("/net/hadess/SensorProxy")
		object.introspect
		@interface = object["net.hadess.SensorProxy"]
		@dbus_interface = object["org.freedesktop.DBus.Properties"]
		@listening_to = []

		@dbus_interface.on_signal("PropertiesChanged") do |source, properties|
			handle_update(properties)
		end

		init_accel

		if @listening_to.length > 0 then
			DBus::Main.new.tap do |loop|
				loop << @bus
				loop.run
			end
		end
	end

	private

	def init_accel()
		if @interface["HasAccelerometer"] then
			#puts "→ Claiming accelerometer."
			@interface.ClaimAccelerometer()
			set_orientation(@interface["AccelerometerOrientation"])
			@listening_to << :accel
		end
	end

	def set_orientation(orientation)
		@orientation = orientation
		#puts "→ Setting orientation to #{orientation}."
		Zr::Hardware::Monitors.refresh
		Zr::Hardware::Monitors.internal_displays.each do |name, monitor|
			monitor.rotate(RANDR[orientation])
			Zr::Hardware::Inputs.touchscreens.each do |id, pointer|
				pointer.map_to_output(name)
			end
		end
	end

	#def init_light()
	#	if interface["HasAmbientLight"] then
	#		interface.ClaimLight()
	#		set_light(interface["LightLevel"])
	#		@listening_to << :light
	#	end
	#end

	def handle_update(properties)
		#["net.hadess.SensorProxy", {"AccelerometerOrientation"=>"right-up"}, []]
		#["net.hadess.SensorProxy", {"LightLevelUnit"=>"lux", "LightLevel"=>5.0}, []]
		if properties["AccelerometerOrientation"] then
			set_orientation(properties["AccelerometerOrientation"])
		end
	end
end
